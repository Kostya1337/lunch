//
//  ViewController.swift
//  Lunch
//
//  Created by kk on 20.06.17.
//  Copyright © 2017 Konstantin Kiski. All rights reserved.
//
import Foundation
import GoogleAPIClientForREST
import GoogleSignIn
import Alamofire
import UIKit
import OAuth2

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    @IBOutlet weak var tableView: UITableView!

    let kClientID = "504738676795-nutu1rmokl2qvkvim9m7ttcfse13i1tm.apps.googleusercontent.com"
    let delegate = UIApplication.shared.delegate as! AppDelegate
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheets]
    var surnameArray = [String](repeating: "", count: 5)
    var rows: [[String]] = [[]]
    var name: [[String]] = [[]]
    var welcomeText = ""
    var surname = ""
    var user: String?
    var firstName: String?
    var lastName: String?

    

    private let service = GTLRSheetsService()
    let signInButton = GIDSignInButton()
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var welcome: UITextView!
    @IBOutlet weak var changeFamily: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.disconnectButton.isHidden = true
        user = delegate.userID
        firstName = delegate.givenName
        lastName = delegate.familyName
        
        let stepperArray = [stepper1, stepper2, stepper3, stepper4, stepper5, stepper6]
        
        for stepper in stepperArray {
            stepper?.wraps = true
            stepper?.autorepeat = true
            stepper?.maximumValue = 5
        }
        
        // Configure Google Sign-in.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/spreadsheets")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        //GIDSignIn.sharedInstance().signInSilently()
 
        // Add the sign-in button.
        view.addSubview(signInButton)

    }

    @IBOutlet weak var value1: UITextView!
    @IBOutlet weak var soup: UITextView!
    @IBOutlet weak var stepper1: UIStepper!
    @IBAction func stepper1(_ sender: UIStepper) {
        value1.text = Int(sender.value).description
    }
    
    @IBOutlet weak var value2: UITextView!
    @IBOutlet weak var potatoes: UITextView!
    @IBOutlet weak var stepper2: UIStepper!
    @IBAction func stepper2(_ sender: UIStepper) {
        value2.text = Int(sender.value).description
    }
    
    @IBOutlet weak var value3: UITextView!
    @IBOutlet weak var fish: UITextView!
    @IBOutlet weak var stepper3: UIStepper!
    @IBAction func stepper3(_ sender: UIStepper) {
        value3.text = Int(sender.value).description
    }
    
    
    @IBOutlet weak var value4: UITextView!
    @IBOutlet weak var saladS: UITextView!
    @IBOutlet weak var stepper4: UIStepper!
    @IBAction func stepper4(_ sender: UIStepper) {
        value4.text = Int(sender.value).description
    }
    
    @IBOutlet weak var value5: UITextView!
    @IBOutlet weak var juiceS: UITextView!
    @IBOutlet weak var stepper5: UIStepper!
    @IBAction func stepper5(_ sender: UIStepper) {
        value5.text = Int(sender.value).description
    }
    
    @IBOutlet weak var value6: UITextView!
    @IBOutlet weak var breadS: UITextView!
    @IBOutlet weak var stepper6: UIStepper!
    @IBAction func stepper6(_ sender: UIStepper) {
        value6.text = Int(sender.value).description
    }
    
    @IBAction func requestButton(_ sender: Any) {
        let parameters: Parameters = [
            "valueInputOption": "USER_ENTERED",
            "data": [
                "range": "22/6!B2:G6",
                "values": [
                    [
                        "0", "0", "0", "0", "Juice", "Bread"
                    ]
                ]
            ]
        ]
        let urlString = "https://sheets.googleapis.com/v4/spreadsheets/1amXko9TGBxJSqxRHSDXcUpAEO3P7idE52-p-CiviJUo/values:batchUpdate"
        let headers    = [ "Content-Type" : "application/json"]
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers : headers)
            .responseJSON { response in
                print(response)
                print(response.result)
        }

    }
    
    @IBAction func refreshButton(_ sender: Any) {
        changeFamily.text = "Обновляем данные"
        print("TOKEN: \(Settings.shared.AuthToken) ")
        listMajors()
        
        
    }
    
    @IBAction func didTapDisconnect(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().disconnect()
        self.disconnectButton.isHidden = true
        self.signInButton.isHidden = false
        self.changeFamily.isHidden = true
        welcome.text = "Выход выполнен!\nАвторизируйтесь, чтобы внести изменения."
        
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        welcome.text = "Добро пожаловать! Войдите,  чтобы загузить данные!"
        self.disconnectButton.isHidden = true
        self.signInButton.isHidden = false
        self.changeFamily.isHidden = false

        if let error = error {
            showAlert("Authentication Error", message: error.localizedDescription)
            self.service.authorizer = nil
        } else {
            self.signInButton.isHidden = true
            self.disconnectButton.isHidden = false
            welcome.text = "Добро пожаловать!"
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            listMajors()
        }
    }
    

    func listMajors() {
        let spreadsheetId = "1amXko9TGBxJSqxRHSDXcUpAEO3P7idE52-p-CiviJUo"
        let range = "\(Settings.shared.selectedDay)/\(Settings.shared.selectedMounth)!A1:G6"
        //let range = "22/6!A1:G6"
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet
            .query(withSpreadsheetId: spreadsheetId, range:range)
        service.executeQuery(query,
                             delegate: self,
                             didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:))
        )
        print(range)
    }
    
    // Process the response and display output
    func displayResultWithTicket(ticket: GTLRServiceTicket,
                                 finishedWithObject result : GTLRSheets_ValueRange,
                                 error : NSError?) {
        let rows = result.values!
        
        soup.text = rows[0][1] as! String
        potatoes.text = rows[0][2] as! String
        fish.text = rows[0][3] as! String
        saladS.text = rows[0][4] as! String
        juiceS.text = rows[0][5] as! String
        breadS.text = rows[0][6] as! String
        value1.text = rows[Settings.shared.selectedRow][1] as! String
        value2.text = rows[Settings.shared.selectedRow][2] as! String
        value3.text = rows[Settings.shared.selectedRow][3] as! String
        value4.text = rows[Settings.shared.selectedRow][4] as! String
        value5.text = rows[Settings.shared.selectedRow][5] as! String
        value6.text = rows[Settings.shared.selectedRow][6] as! String
        
        surname = rows[Settings.shared.selectedRow][0] as! String

        changeFamily.text = ("Вы отмечаете обед за \(surname)a\nДата:\(Settings.shared.selectedDay)/\(Settings.shared.selectedMounth)")
        

    }
    
    func showAlert(_ title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
}
