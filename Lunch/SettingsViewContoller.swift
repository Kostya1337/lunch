//
//  SettingsVoewContoller.swift
//  Lunch
//
//  Created by kk on 20.06.17.
//  Copyright © 2017 Konstantin Kiski. All rights reserved.
//

//import GoogleAPIClientForREST
//import GoogleAPIClient
//import GoogleSignIn
import UIKit
import Foundation

class SettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var arr:NSArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
        arr = [1,2,3,4,5]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(arr[row])"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
}
