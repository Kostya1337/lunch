//
//  Settings.swift
//  Lunch
//
//  Created by kk on 20.06.17.
//  Copyright © 2017 Konstantin Kiski. All rights reserved.
//

class Settings {
    static let shared = Settings()
    var selectedRow: Int = 1
    var family: String = ""
    var name: [[String]] = [[]]
    var selectedDay: String = "1"
    var selectedMounth: String = "1"
    var AuthToken = ""

}
