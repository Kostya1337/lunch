//
//  SettingsViewController.swift
//  Lunch
//
//  Created by kk on 20.06.17.
//  Copyright © 2017 Konstantin Kiski. All rights reserved.
//

import Foundation
import UIKit
import GoogleAPIClientForREST
import GoogleSignIn
import UIKit

class SettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, GIDSignInDelegate, GIDSignInUIDelegate  {
    
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheets]
    private let service = GTLRSheetsService()
    var arr: [String] = []
    var rows: [[String]] = [[]]
    var name = Array(repeating: "Фамилия", count: 6)
    var day = Array(repeating: "1", count: 31)
    var mounth = Array(repeating: "1", count: 12)
    
    let signInButton = GIDSignInButton()
    @IBOutlet weak var surnamePickerView: UIPickerView!
    @IBOutlet weak var datePickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        //view.addSubview(signInButton)

        for i in 0...30 {
            day[i] = String(i+1)
        }
        for i in 0...11 {
            mounth[i] = String(i+1)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            self.service.authorizer = nil
        } else {
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            listMajors()
        }
    }
    
    func listMajors() {
        let spreadsheetId = "1amXko9TGBxJSqxRHSDXcUpAEO3P7idE52-p-CiviJUo"
        let range = "\(Settings.shared.selectedDay)/\(Settings.shared.selectedMounth)!A1:G6"
        //let range = "22/6!A1:G6"
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet
            .query(withSpreadsheetId: spreadsheetId, range:range)
        service.executeQuery(query,
                             delegate: self,
                             didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:))
        )
    }
    
    // Process the response and display output
    func displayResultWithTicket(ticket: GTLRServiceTicket,
                                 finishedWithObject result : GTLRSheets_ValueRange,
                                 error : NSError?) {
        
        if let error = error {
            showAlert(title: "Error", message: error.localizedDescription)
            return
        }
        
        let rows = result.values!
        for i in 0...5{
            name[i] = rows[i][0] as! String
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if (pickerView.tag == 1) {
            return 1
        } else {
            return 2
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView.tag == 1) {
            return name.count
        } else {
            if (component == 0) {
                return day.count
            } else {
                return mounth.count
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView.tag == 1) {
            return "\(name[row])"
        } else {
            if (component == 0) {
                return "\(day[row])"
            } else {
                return "\(mounth[row])"
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if (pickerView.tag == 1) {
            Settings.shared.selectedRow = row
        } else {
            if (component == 0) {
                Settings.shared.selectedDay =  "\(day[row])"
            } else {
                Settings.shared.selectedMounth = "\(mounth[row])"
            }
        }
    }
    
    func pickerView2(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        let data = mounth[row]
        let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 36.0, weight: UIFontWeightRegular)])
        label?.attributedText = title
        label?.textAlignment = .center
        label?.textColor = UIColor.white
        
        return label!
    }
    
    func pickerView1(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        let data = day[row]
        let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 36.0, weight: UIFontWeightRegular)])
        label?.attributedText = title
        label?.textAlignment = .center
        label?.textColor = UIColor.white
    
        return label!
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        var label1 = view as! UILabel!
        if label1 == nil {
            label1 = UILabel()
        }
        var label2 = view as! UILabel!
        if label2 == nil {
            label2 = UILabel()
        }
        
        if (pickerView.tag == 1) {

            let data = name[row]
            let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 36.0, weight: UIFontWeightRegular)])
            label?.attributedText = title
            label?.textAlignment = .center
            label?.textColor = UIColor.white
        
        } else {
            if (component == 0) {
                let data = day[row]
                let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 36.0, weight: UIFontWeightRegular)])
                label?.attributedText = title
                label?.textAlignment = .center
                label?.textColor = UIColor.white
            } else {
                let data = mounth[row]
                let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 36.0, weight: UIFontWeightRegular)])
                label?.attributedText = title
                label?.textAlignment = .center
                label?.textColor = UIColor.white
            
            }
        }
        return label!
    }
    func showAlert(title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
}
